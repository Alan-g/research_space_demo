# Research Space Demo application

I create a Spring MVC application who takes the sample list on RSpace Api.

## Design:

I chosed to make an MVC application for a better display of the data.\
The application consists of several parts:

- Controller (com.research.space.demo.controller): using the logic of the application. On this application there are only two controllers (sample and sampleDetails) explained below.
- View (resources/template): composed of Html Pages for display. The controller  "sample" is used to view the display of all samples and the "sampleDetails" view for the details of one sample.
- Service (com.research.space.demo.service): composed of classes that communicate with the API. I created an abstract class called "Service". On this class, there is the base URL of the API and the API key is retrieve from "application.properties". This way, all the other service classes are extend from this class and can access this data. The "SampleService" class is used to retrieve the sample list and only one sample using their ID.
- model (com.research.space.demo.model): composed of the application classes. There are two classes: "Sample" and "SampleList". "Sample" is composed of different fields selected (name, description,...). "SampleList" is composed of the "Sample" object and two other fields which are the page number and the total number of items

## Installation:

For run the project you need to have Java 11.

1- Open a cmd in the project directory

2- You must build the project and load the dependencies with the following command
```
./mvnw package
```

3- Once the loading is finished you have two options:

Start the project with maven and the following command:
```
./mvnw spring-boot:run
```
or go to the "target" directory and execute the following command:
```
java -jar demo-0.0.1-SNAPSHOT.jar
```

you can now access the application at this address :
http://localhost:8080/sample






