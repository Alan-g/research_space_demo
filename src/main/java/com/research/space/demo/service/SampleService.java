package com.research.space.demo.service;

import com.research.space.demo.model.SampleList;
import com.research.space.demo.model.Sample;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Component
public class SampleService  extends Service{


    private final RestTemplate restTemplate;
    public SampleService() {

        this.restTemplate = new RestTemplate();
    }

    public final SampleList getAll(final int pageNumber) throws HttpClientErrorException {
        return this.restTemplate.exchange(this.baseUri+ "/samples?pageNumber="+pageNumber+"&pageSize=20&orderBy=name asc", HttpMethod.GET, headers() , SampleList.class).getBody();
    }

    public final Sample getOne(final int id) throws HttpClientErrorException{
        StringBuilder stringBuilder = new StringBuilder();
        Map<String, String> params = new HashMap<String, String>();
        params.put("id", stringBuilder.append(id).toString());

        return this.restTemplate.exchange(this.baseUri+"/samples/{id}", HttpMethod.GET, headers() ,Sample.class,params).getBody();
    }

}
