package com.research.space.demo.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public abstract class Service {

    @Value("${apiKey}")
    private String apiKey;

    @Value("${apiUrl}")
    protected String baseUri;

    protected HttpEntity headers(){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("apiKey",apiKey);
        return new HttpEntity(headers);
    }
}
