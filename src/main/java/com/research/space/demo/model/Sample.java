package com.research.space.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.jdi.IntegerValue;

import javax.swing.text.DateFormatter;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class Sample {

    @JsonProperty("id")
    private int id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("description")
    private String description;

    @JsonProperty("expiryDate")
    private Date expiryDate;

    @JsonProperty("type")
    private String type;

    @JsonProperty("sampleSource")
    private String source;

    private double storageTempMax;

    private double storageTempMin;

    private int quantity;

    public Sample() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getExpiryDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(expiryDate);
    }

    public String getType() {
        return type;
    }

    public String getSource() {
        return source;
    }

    public double getStorageTempMax() {
        return storageTempMax;
    }

    public double getStorageTempMin() {
        return storageTempMin;
    }

    @JsonProperty("storageTempMin")
    public void setStorageTempMin(Map<String, Object> storageTempMin) {
        this.storageTempMin = (double) storageTempMin.get("numericValue");
    }

    @JsonProperty("storageTempMax")
    public void setStorageTempMax(Map<String, Object> storageTempMin) {
        this.storageTempMax = (double) storageTempMin.get("numericValue");
    }

    @JsonProperty("quantity")
    public void setQuantity(Map<String, Object> storageTempMin) {
        double value = (double) storageTempMin.get("numericValue");
        this.quantity = (int) value;
    }

    public int getQuantity() {
        return quantity;
    }
}
