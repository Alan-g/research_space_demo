package com.research.space.demo.model;

import java.util.List;

public class SampleList {

    private List<Sample> samples;
    private int totalHits;
    private int pageNumber;

    public SampleList(List<Sample> samples, int totalHits, int pageNumber) {
        this.samples = samples;
        this.totalHits = totalHits;
        this.pageNumber = pageNumber;
    }

    public SampleList() {
    }

    public List<Sample> getSamples() {
        return samples;
    }

    public int getTotalHits() {
        return totalHits;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setSamples(List<Sample> samples) {
        this.samples = samples;
    }

    public void setTotalHits(int totalHits) {
        this.totalHits = totalHits;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }
}
