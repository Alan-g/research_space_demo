package com.research.space.demo.controller;

import com.research.space.demo.model.SampleList;
import com.research.space.demo.model.Sample;
import com.research.space.demo.service.SampleService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.HttpClientErrorException;

@Controller
public class SampleController {

    private final SampleService sampleService;

    public SampleController(final SampleService sampleService) {
        this.sampleService = sampleService;
    }

    @GetMapping("/sample")
    public String sample(Model model) {
        try {
            SampleList listModel = this.sampleService.getAll(0);
            model.addAttribute("samples", listModel.getSamples());
            model.addAttribute("pageNumber", listModel.getPageNumber());
            model.addAttribute("totalHits", listModel.getTotalHits());
            model.addAttribute("error", false);
        } catch (HttpClientErrorException e) {
            e.printStackTrace();
            model.addAttribute("error", true);
            model.addAttribute("errorMessage", e.getStatusCode());
        }
        return "sample";
    }

    @GetMapping("/sample/{id}")
    public String sampleById(@PathVariable final int id,Model model){
        try {
            final Sample sample = this.sampleService.getOne(id);
            model.addAttribute("sample", sample);
        } catch (HttpClientErrorException e) {
            e.printStackTrace();
        }

        return "sampleDetails";
    }
}
